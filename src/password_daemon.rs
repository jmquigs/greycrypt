// all of this is unused on windows, pending port

#[cfg(not(target_os = "windows"))]
extern crate libc;
#[cfg(not(target_os = "windows"))]
use std::process;
#[cfg(not(target_os = "windows"))]
use std::thread;
#[cfg(not(target_os = "windows"))]
use std::fs;

#[cfg(not(target_os = "windows"))]
use std::fs::{create_dir_all};
#[cfg(not(target_os = "windows"))]
use std::os::unix::fs::PermissionsExt;
#[cfg(not(target_os = "windows"))]
use std::io::{Read,Write};
#[cfg(not(target_os = "windows"))]
use std::env;
#[cfg(not(target_os = "windows"))]
use std::path::{PathBuf};
extern crate time;

#[cfg(not(target_os = "windows"))]
extern crate unix_socket;
#[cfg(not(target_os = "windows"))]
use self::unix_socket::{UnixStream, UnixListener};
#[cfg(not(target_os = "windows"))]
use std::net::Shutdown;

#[cfg(not(target_os = "windows"))]
fn get_socket_dir() -> String {
    let mut dir = env::temp_dir();
    dir.push("pd_socket");
    dir.to_str().unwrap().to_owned()
}

#[cfg(not(target_os = "windows"))]
fn get_socket_name() -> String {
    let mut dir = PathBuf::from(get_socket_dir());
    dir.push("__greycrypt_password_socket__");
    dir.to_str().unwrap().to_owned()
}

#[cfg(not(target_os = "windows"))]
fn write_to_daemon(req:&str) -> Option<String> {
    let mut stream = match UnixStream::connect(get_socket_name()) {
        Err(_) => {
            // this is normal if it isn't running
            //println!("pd connect error: {}", e);
            return None;
        }
        Ok(s) => s
    };

    //println!("writing req to pd: {}", req);
    match writeln!(stream, "{}", req) {
        Err(e) => println!("Failed to write pd request: {}", e),
        Ok(_) => ()
    }
    match stream.shutdown(Shutdown::Write) {
        Err(e) => println!("Failed to shutdown pd request stream: {}", e),
        Ok(_) => ()
    }

    //println!("awaiting reply");
    let mut response = String::new();
    match stream.read_to_string(&mut response) {
        Err(_) => None,
        Ok(_) => {
            // treat empty responses as no response
            if response.trim() == "" {
                None
            } else {
                Some(response)
            }
        }
    }
}

#[cfg(not(target_os = "windows"))]
fn cleanup() {
    let _ = fs::remove_file(get_socket_name());
}

#[cfg(not(target_os = "windows"))]
pub fn start_timer(keep_alive_time_secs:i64) {
    //println!("starting timer for {} secs", keep_alive_time_secs);
    thread::spawn(move || {
        let mut remaining = keep_alive_time_secs;
        while remaining > 0 {
            thread::sleep_ms(1000);
            remaining = remaining - 1;
        }
        write_to_daemon("timerquit");
    });
}

#[cfg(not(target_os = "windows"))]
pub fn start(password:&str,keep_alive_time_secs:i64) {
    let forkres = unsafe { libc::fork() };
    match forkres {
        0 => {
            // child process

            // remove previous socket, if any
            cleanup();

            // make sure socket dir exists and has correct permissions
            {
                let sd = get_socket_dir();
                let pbd = PathBuf::from(&sd);
                if !pbd.is_dir() {
                    match fs::create_dir_all(&sd) {
                        Err(e) => panic!("Failed to create pd directory: {}", e),
                        Ok(_) => ()
                    }
                }

                match fs::metadata(&sd) {
                    Err(e) => panic!("failed to read socket permissions: {}", e),
                    Ok(md) => {
                        let mut permissions = md.permissions();
                        permissions.set_mode(0o700);
                        match fs::set_permissions(get_socket_dir(), permissions) {
                            Err(e) => panic!("Failed to set socket permissions: {}", e),
                            Ok(_) => ()
                        }
                    }
                };
            }

            // make socket
            let listener = UnixListener::bind(get_socket_name()).unwrap();

            //println!("bound to {}", get_socket_name());

            start_timer(keep_alive_time_secs);

            let mut time_since_last_client:Option<time::Timespec> = None;

            // accept connections and process them, spawning a new thread for each one
            for stream in listener.incoming() {
                let mut stream = match stream {
                    Ok(stream) => stream,
                    Err(e) => {
                        /* connection failed */
                        panic!("Failed to connect to client: {}", e);
                    }
                };
                let mut req = String::new();
                match stream.read_to_string(&mut req) {
                    Err(e) => panic!("Failed to read client request: {}", e),
                    Ok(s) => s
                };
                let req = req.trim();
                //println!("pd req: {}", req);
                let req = &req[..];
                match req {
                    "gimmie" => {
                        let password = password.to_owned();
                        time_since_last_client = Some(time::get_time());

                        thread::spawn(move || {
                            //println!("got client");
                            match write!(stream, "{}", password) {
                                Err(e) => panic!("Failed to write pw to client stream: {}", e),
                                Ok(_) => ()
                            }

                            match stream.shutdown(Shutdown::Write) {
                                Err(e) => panic!("Failed to shutdown client stream: {}", e),
                                Ok(_) => ()
                            }
                        });
                    },
                    "timerquit" => {
                        // quit if no client has connected in the keep alive time;
                        // otherwise, start a new timer
                        match time_since_last_client {
                            None => process::exit(0),
                            Some (t) => {
                                let now = time::get_time();
                                let elapsed = now.sec - t.sec;
                                if elapsed >= (keep_alive_time_secs) {
                                    //println!("exiting pd because no time remains");
                                    cleanup();
                                    process::exit(0);
                                } else {
                                    let remaining = keep_alive_time_secs - elapsed;
                                    //println!("keeping pd alive, {} seconds remain", remaining);
                                    start_timer(remaining);
                                }
                            }
                        }
                    },
                    _ => {
                        panic!("password daemon: unhandled request: {}", req);
                    }
                }

            }
            process::exit(0);
        }
        n if n < 0 => panic!("Error forking: {}", n),
        _ => ()
    }
}

#[cfg(not(target_os = "windows"))]
pub fn get_password() -> Option<String> {
    write_to_daemon("gimmie")
}

#[cfg(target_os = "windows")]
pub fn start(_:&str,_:i64) {
    println!("Password daemon not yet available on this platform");
}

#[cfg(target_os = "windows")]
pub fn get_password() -> Option<String> {
    None
}
