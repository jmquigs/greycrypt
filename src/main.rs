#![feature(plugin)]
#![plugin(clippy)]
#![allow(let_and_return)]
// these loop lints seems a bit buggy as of 8/16/2015
#![allow(needless_range_loop)]
#![allow(explicit_iter_loop)]

#[macro_use]
extern crate log;

mod util;
mod config;
mod mapping;
mod syncfile;
mod crypto_util;
mod syncdb;
mod core;
mod commands;
mod trash;
mod logging;
mod password_daemon;
mod process_mutex;

#[cfg(test)]
mod testlib;

use std::thread;

extern crate getopts;
extern crate rpassword;

use getopts::Options;
use std::env;

#[allow(dead_code)]
fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

#[allow(dead_code)]
fn main() {
    // parse command line
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    let mut opts = Options::new();
    opts.optopt("s", "", "show syncfile metadata", "SYNC_FILE_PATH");
    opts.optopt("t", "", "set poll interval (in seconds)", "POLL_INTERVAL");
    opts.optopt("c", "", "use a different configuration file", "CONFIG_FILE_PATH");
    opts.optflagopt("d", "", "Unix only. Start a password daemon that lives for the specified amount of \
        time after each client request; while active, you won't need to re-enter the encryption password each
        time you start the program.  Note that this reduces security, since any program \
        running as your user or an administrator can read the password if it knows where to \
        look.  If you CTRL-C out of poll mode, any daemon launched by the same process will be killed.", "[optional keep alive time in seconds, default 30]");
    opts.optopt("", "add-ignore", "add entry to ignore list", "'GLOB_PATTERN'");
    opts.optopt("", "remove-ignore", "remove entry from ignore list", "'GLOB_PATTERN'");
    opts.optflag("", "show-ignore", "show the ignore list for current sync dir");
    opts.optflag("", "show-unmapped", "show a list of syncfiles that are not mapped on this machine");
    opts.optopt("", "remove-unmapped", "remove unmapped syncfiles matching pattern; will prompt for confirmation", "'GLOB_PATTERN'");
    opts.optflag("", "show-unmapped-dirs", "show a list of directories containing syncfiles that are not mapped on this machine");
    opts.optflag("x", "", "show syncfile metadata for all conflicted files");
    opts.optflag("v", "", "use verbose logging");
    opts.optflag("p", "", "change encryption password");
    opts.optflag("h", "help", "print this help menu");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    let log_util = {
        let level =
            if matches.opt_present("v") {
                Some(log::LogLevelFilter::Trace)
            } else {
                None // use default
            };
        match logging::init(level) {
            Err(e) => panic!("Failed to init logger: {}", e),
            Ok(l) => l
        }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }

    let cfile = {
        if matches.opt_present("c") {
            match matches.opt_str("c") {
                None => return print_usage(&program,opts),
                Some (i) => {
                    info!("Using configuration file: {}", i);
                    Some (i.to_owned())
                }
            }
        } else {
            None
        }
    };

    let changing_password = matches.opt_present("p");
    let hn_override = None;

    let pd_ttl =
        if matches.opt_present("d") {
            match matches.opt_str("d") {
                None => Some(30),
                Some (i) => match u32::from_str_radix(&i,10) {
                    Err(e) => panic!("Unable to parse password daemon lifetime: {}", e),
                    Ok(i) => Some(i)
                }
            }
        } else {
            None
        };

    let get_password = || {
        // always prompt when changing password, don't use the daemon
        if changing_password {
            return config::pw_prompt(Some("Enter old password:"));
        }

        // try to get password from password daemon
        let daemon_pw = password_daemon::get_password();
        match daemon_pw {
            Some(pw) => {
                info!("Retrieved cached password from password daemon");
                pw
            },
            None => {
                let pw = config::pw_prompt(None);

                match pd_ttl {
                    None => (),
                    Some(ttl) => {
                        info!("Starting password daemon; TTL {} seconds after each request", ttl);
                        password_daemon::start(&pw,ttl as i64);
                    }
                }

                pw
            }
        }
    };

    // init conf and state
    let conf = config::parse(cfile,hn_override,get_password);
    let syncdb = match syncdb::SyncDb::new(&conf) {
        Err(e) => panic!("Failed to create syncdb: {:?}", e),
        Ok(sdb) => sdb
    };

    let mutex = match process_mutex::acquire(conf.sync_dir()) {
        Err(e) => panic!("Failed to create process mutex: {}", e),
        Ok(f) => f
    };
    let _ = mutex; // "fix" unused var warning, we just want it to hang out until it goes out of scope

    let mut state = core::SyncState::new(conf,syncdb,log_util);

    // process other args

    if matches.opt_present("show-unmapped") {
        commands::show_unmapped(&mut state, false);
        return;
    }
    if matches.opt_present("show-unmapped-dirs") {
        commands::show_unmapped(&mut state, true);
        return;
    }
    if matches.opt_present("remove-unmapped") {
        match matches.opt_str("remove-unmapped") {
            None => return print_usage(&program,opts),
            Some(s) => commands::remove_unmapped(&mut state, &s, false)
        }
        return;
    }

    // ignore list stuff
    if matches.opt_present("show-ignore") {
        commands::show_ignore_list(&state);
        return;
    }
    if matches.opt_present("add-ignore") {
        match matches.opt_str("add-ignore") {
            None => return print_usage(&program,opts),
            Some(s) => commands::add_to_ignore_list(&mut state, &s)
        }
        commands::show_ignore_list(&state);
        return;
    }
    if matches.opt_present("remove-ignore") {
        match matches.opt_str("remove-ignore") {
            None => return print_usage(&program,opts),
            Some(s) => commands::remove_from_ignore_list(&mut state, &s)
        }
        commands::show_ignore_list(&state);
        return;
    }

    let poll_interval =
        if matches.opt_present("t") {
            match matches.opt_str("t") {
                None => return print_usage(&program,opts),
                Some (i) => match u32::from_str_radix(&i,10) {
                    Err(e) => panic!("Unable to parse poll interval: {}", e),
                    Ok(i) => i
                }
            }
        } else {
            3
        };

    if matches.opt_present("s") {
        // inspect syncfile
        match matches.opt_str("s") {
            None => print_usage(&program,opts),
            Some (filename) => {
                commands::show_syncfile_meta(&mut state,&filename);
            }

        }
    }
    else if changing_password {
        commands::change_password(&mut state);
    }
    else if matches.opt_present("x") {
        commands::show_conflicted_syncfile_meta(&mut state);
    }
    else {
        // run standard sync in loop
        info!("Starting");
        info!("Poll interval: {} seconds", poll_interval);

        loop {
            core::do_sync(&mut state);
            thread::sleep_ms(poll_interval * 1000);
        }
    }
}
