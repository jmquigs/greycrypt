//use std::fs::{PathExt,remove_file,remove_dir,read_dir};
use std::path::{PathBuf};
use std::collections::HashSet;
use std::io;
use std::fs;
// use std::collections::HashMap;
// use std::cmp::Ordering;

use config;
use syncfile;
use core;
use util;

extern crate glob;

#[allow(dead_code)]
pub fn show_syncfile_meta(state: &mut core::SyncState, filename:&str) {
    let syncpath = PathBuf::from(filename);
    let mdhash = match syncfile::SyncFile::get_metadata_hash(&state.conf,&syncpath) {
        Err(e) => panic!("{}", e),
        Ok(hash) => hash
    };

    let mut keys:Vec<String> = Vec::new();
    for (k,_) in &mdhash {
        keys.push(k.to_owned());
    }
    keys.sort();
    for k in keys {
        let v = mdhash.get(&k).unwrap();
        println!("{}: {}", k, v);
    }

    let mut sf = syncfile::SyncFile::from_syncfile_panic(&state.conf,&syncpath);
    let entry = state.syncdb.get(&sf);
    match entry {
        None => println!("No sync db entry (file has not yet been processed on this machine)"),
        Some(entry) => {
            println!("syncdb entry:");
            println!("  for this file: {}", entry.revguid == sf.revguid);
            println!("  native_mtime: {}", entry.native_mtime);
            println!("  revguid: {}", entry.revguid);
        }
    }

    let mut data:Vec<u8> = Vec::new();

    match sf.decrypt_to_writer(&state.conf, &mut data) {
        Err(e) => panic!("Error {:?}", e),
        Ok(_) => {
            println!("decrypted size: {}", data.len());

            if !sf.is_binary {
                println!("text:");
                println!("{}", String::from_utf8(data).unwrap());
            } else {
                println!("binary file data omitted");
            }
        }
    }
}

pub fn show_unmapped(state: &mut core::SyncState, dirs_only:bool) {
    state.sync_files_for_id = core::find_all_syncfiles(state);
    let (_,mut unmapped) = core::filter_syncfiles(state);
    core::sort_syncfiles(state, &mut unmapped);

    if !dirs_only {
        println!("unmapped sync files (no native path specified in conf):");
    } else {
        println!("unmapped sync directories (no native path specified in conf):");
    }

    let mut unmapped_set:HashSet<String> = HashSet::new();

    for f in &unmapped {
        let pb = PathBuf::from(f);
        let sf = state.sync_file_cache.get(&state.conf,&pb);
        if !dirs_only {
            println!("{} ; keyword: {}; sid: {})", sf.relpath, sf.keyword, sf.id);
        } else {
            match util::get_relpath_parent(&sf.relpath) {
                None => unmapped_set.insert(sf.relpath.to_owned()),
                Some(par) => unmapped_set.insert(par) 
            };
        }
    }

    // this is only filled in the dirs_only case
    let mut sorted:Vec<String> = Vec::new();
    for f in &unmapped_set {
        sorted.push(f.to_owned());
    }
    sorted.sort();
    for f in &sorted {
        println!("{}", f);
    }
}

pub fn remove_unmapped(state: &mut core::SyncState, entry:&str, force:bool) {
    let entry = entry.trim();

    let pat = match glob::Pattern::new(entry) {
        Err(e) => panic!("Failed to parse glob pattern: {}", e),
        Ok(pat) => pat
    };

    state.sync_files_for_id = core::find_all_syncfiles(state);
    let (_,mut unmapped) = core::filter_syncfiles(state);
    core::sort_syncfiles(state, &mut unmapped);

    let mut to_remove:Vec<(String,String)> = Vec::new();
    for f in &unmapped {
        let pb = PathBuf::from(f);
        let sf = state.sync_file_cache.get(&state.conf,&pb);

        if pat.matches(&sf.relpath) {
            to_remove.push((sf.relpath.to_owned(), f.to_owned()));
        }
    }

    if to_remove.is_empty() {
        println!("no matches for pattern {}", pat);
    } else {
        println!("removing syncfiles for:");
        for f in &to_remove {
            let relpath = &f.0;
            let syncfile = &f.1;
            println!("{} (syncfile: {})", &relpath, &syncfile);
        }
        
        let remove_them = || {
            for f in &to_remove {
                let syncfile = &f.1;
                match fs::remove_file(&syncfile) {
                    Err(e) => warn!("Failed to remove file: {}; Error: {}", &syncfile, e),
                    Ok(_) => ()
                }
            }
            println!("{} syncfiles removed", to_remove.len());        
        };

        if !force {
            // prompt for confirmation
            println!("");
            println!("Type yes/y/ok to confirm:");
            let mut line = String::new();
            match io::stdin().read_line(&mut line) {
                Err(e) => panic!("failed to read line: {}", e),
                Ok(_) => ()
            }
            let line = line.trim();

            match line {
                "y"
                | "yes"
                | "ok" => remove_them(),
                _ => println!("Aborted")
            }

        } else {
            remove_them();        
        }
    }
}

#[allow(dead_code)]
pub fn show_conflicted_syncfile_meta(state: &mut core::SyncState) {
    state.sync_files_for_id = core::find_all_syncfiles(state);
    let mut sync_files:Vec<String> = Vec::new();

    for (sid,files) in &state.sync_files_for_id {
        if state.is_conflicted(sid) {
            for f in files {
                sync_files.push(f.to_owned());
            }
        }
    }

    for f in sync_files {
        println!("Showing conflicts (NOTE: dedup not run)");
        println!("Conflicted file: {}", f);
        show_syncfile_meta(state, &f);
        println!("");
    }
}

#[cfg(not(test))]
fn collect_new_password() -> String {
    let new = config::pw_prompt(Some("Enter new password:"));
    let new2 = config::pw_prompt(Some("Confirm new password:"));

    if new != new2 {
        panic!("New passwords do not match");
    }
    new
}

#[cfg(test)]
fn collect_new_password() -> String {
    "swordfish".to_owned()
}

pub fn change_password(state: &mut core::SyncState) {
    let new_password = collect_new_password();

    let new_conf = state.conf.with_encryption_key(Some(config::get_encryption_key(&new_password)));

    let syncfiles = core::find_all_syncfiles(state);

    let mut count = 0;
    for (_,files) in syncfiles.iter() {
        for f in files.iter() {
            // try to decode with new pw
            let syncfile = PathBuf::from(&f);
            let mut sf = match syncfile::SyncFile::from_syncfile(&new_conf,&syncfile) {
                Ok(_) => continue, // already updated
                Err(_) => {
                    // try old pw
                    match syncfile::SyncFile::from_syncfile(&state.conf,&syncfile) {
                        Err(e) => {
                            // fail, log and skip
                            warn!("Failed to decode syncfile with old & new password, skipping: {}; error: {}", f, e);
                            continue;
                        },
                        Ok(sf) => sf
                    }
                }
            };

            let mut data:Vec<u8> = Vec::new();

            match sf.decrypt_to_writer(&state.conf, &mut data) {
                Err(e) => panic!("Error decrypting file data for {}: {:?}", f, e),
                Ok(_) => {
                    // re-encrypt with new conf
                    match sf.save_with_data(&new_conf, Some(syncfile), data) {
                        Err(e) => panic!("Error encrypting file data for {}: {:?}", f, e),
                        Ok(_) => ()
                    }
                }
            }

            count = count + 1;
        }
    }
    state.conf = new_conf;
    info!("Password changed on {} sync files", count);
}

#[allow(dead_code)]
pub fn show_ignore_list(state: &core::SyncState) {
    println!("ignore list for {}:", &state.conf.sync_dir());
    println!("hardcoded:");
    let il = core::get_hard_ignore_list();
    for p in &il {
        println!("{}", p);
    }
    println!("conf:");
    for p in &state.conf.ignore_list {
        println!("{}", p);
    }
}

pub fn add_to_ignore_list(state: &mut core::SyncState, entry:&str) {
    // ignore ignore dups
    let entry = entry.trim();

    match state.conf.ignore_list.iter().find(|f| f.trim() == entry) {
        None => {
            state.conf.ignore_list.push(entry.to_owned());
            state.conf.save_ignore_list();
        },
        Some(_) => ()
    }
}

pub fn remove_from_ignore_list(state: &mut core::SyncState, entry:&str) {
    let entry = entry.trim();

    let removed = state.conf.ignore_list.iter().position(|f| f.trim() == entry).map(|e| state.conf.ignore_list.remove(e));
    if !removed.is_none() {
        state.conf.save_ignore_list();
    }
}

#[cfg(test)]
mod tests {
    use core;
    use testlib::util::{basic_alice_bob_setup,verify_sync_state,write_text_file};
    use std::path::{PathBuf};
    use std::fs;

    #[test]
    fn change_password() {
        let (ref mut alice_mconf, _) = basic_alice_bob_setup("commands_change_password");

        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 2, 2);

        let orig_ek = alice_mconf.state.conf.encryption_key.clone();

        super::change_password(&mut alice_mconf.state);

        assert!(alice_mconf.state.conf.encryption_key != None);
        assert!(alice_mconf.state.conf.encryption_key != orig_ek);

        verify_sync_state(alice_mconf, 2, 2);
    }

    #[test]
    #[should_panic(expected="incorrect password")]
    fn change_password_old_fails() {
        let (ref mut alice_mconf, _) = basic_alice_bob_setup("commands_change_password_old_fails");

        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 2, 2);

        let orig_conf = alice_mconf.state.conf.clone();

        super::change_password(&mut alice_mconf.state);

        alice_mconf.state.conf = orig_conf;

        verify_sync_state(alice_mconf, 2, 2);
    }

    #[test]
    fn ignore_list_persist() {
        let (ref mut alice_mconf, _) = basic_alice_bob_setup("ignore_list_persist");
        super::add_to_ignore_list(&mut alice_mconf.state, "*.gcignore");
        alice_mconf.state.conf.load_ignore_list();
        assert_eq!(alice_mconf.state.conf.ignore_list.len(), 1);
    }

    #[test]
    fn ignore_list() {
        let (ref mut alice_mconf, _) = basic_alice_bob_setup("ignore_list");

        // make an ignore list entry
        super::add_to_ignore_list(&mut alice_mconf.state, "*.gcignore");

        // create an ignored file, sync, should be ignored
        let mut ignore_pb = PathBuf::from(&alice_mconf.native_root);
        ignore_pb.push("docs");
        ignore_pb.push("test_ignore.gcignore");
        write_text_file(ignore_pb.to_str().unwrap(), "TEST");

        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 2, 3); // three native files now, but still only two syncfiles

        // add another entry
        super::add_to_ignore_list(&mut alice_mconf.state, "*.gcignore2");

        let mut ignore_pb = PathBuf::from(&alice_mconf.native_root);
        ignore_pb.push("docs");
        ignore_pb.push("test_ignore.gcignore2");
        write_text_file(ignore_pb.to_str().unwrap(), "TEST");

        // sync again
        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 2, 4);

        // remove entry
        super::remove_from_ignore_list(&mut alice_mconf.state, "*.gcignore");
        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 3, 4);

        // add entry for text files
        super::add_to_ignore_list(&mut alice_mconf.state, "*.txt");
        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 2, 4);

        // remove all entries
        super::remove_from_ignore_list(&mut alice_mconf.state, "*.gcignore");
        super::remove_from_ignore_list(&mut alice_mconf.state, "*.gcignore2");
        super::remove_from_ignore_list(&mut alice_mconf.state, "*.txt");
        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 4, 4);
    }
    
    #[test]
    fn remove_unmapped() {
        let (ref mut alice_mconf, ref mut bob_mconf) = basic_alice_bob_setup("remove_unmapped");

        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 2, 2); 
        
        // make a file that is mapped in alice's config but not bob's
        
        let mut new_path = PathBuf::from(&alice_mconf.native_root);
        new_path.push("new");
        alice_mconf.state.conf.native_paths.push(new_path.to_str().unwrap().to_owned());
        //println!("{:?}", alice_mconf.state.conf.native_paths);
        fs::create_dir(new_path.to_str().unwrap()).unwrap();
        
        new_path.push("test_new_file");
        write_text_file(new_path.to_str().unwrap(), "TEST");
        
        core::do_sync(&mut alice_mconf.state);
        verify_sync_state(alice_mconf, 3, 3);
        
        // sync bob
        core::do_sync(&mut bob_mconf.state);
        verify_sync_state(bob_mconf, 3, 2); 
        
        // should show as unmapped
        let (_,unmapped) = core::filter_syncfiles(&mut bob_mconf.state);
        assert!(unmapped.len() == 1);
        
        // now remove it
        super::remove_unmapped(&mut bob_mconf.state, "*test_new*", true);
        verify_sync_state(bob_mconf, 2, 2);
         
    }
}
