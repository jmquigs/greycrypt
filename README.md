[![Join the chat at https://gitter.im/jmquigs/greycrypt](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/jmquigs/greycrypt?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Greycrypt is a command line program that encrypts and writes your files to a 
cloud provider storage directory.  Since the files are "at rest" encrypted
with your own password-based key, in theory neither the cloud provider nor 
anyone else who lacks the password can examine the file contents.

GreyCrypt doesn't require you to relocate your unencrypted files to 
some shared parent directory.   Instead, you specify a configuration file 
containing directories or files that you want 
to sync, as well as the path to your local cloud storage directory.  After 
starting, Greycrypt prompts for an encryption password, and then copies 
encrypted versions of those files to the cloud storage directory.  You can 
then unpack the files on another machine by running an instance of your 
cloud sync program and GreyCrypt.

You can also choose just a subset of files to unpack on a particular machine;
the cloud provider will still sync them, but GreyCrypt won't generate
local unencrypted copies of them.

Licensed under the MIT license.

### Source Hosting

This repository is dual-hosted; I keep "master" in sync on both, other 
branches may diverge.

* https://bitbucket.org/jmquigs/greycrypt
* https://github.com/jmquigs/greycrypt

### Building 

A rust nightly build is currently required.  Binary releases are not 
yet available.  If you are building on Windows, you must make sure that you 
have a working 64bit MinGW install to build the crypto package.
See https://github.com/DaGenix/rust-crypto/issues/299

The program has two build modes; Developer (Debug) and Release.  These 
versions write to different directories so that they don't interfere with 
each other.  

A release build is highly recommended for production use to reduce encryption
cpu utilization.  To build release in a unixlike environment, run:

```bash
$ sh shtool/relbuild.sh
```

On Windows, if you don't have cygwin or msys, just run the cargo command 
found in shtool/relbuild.

To build the develop version, just run "cargo build".  Or use the included 
build-and-run script
 
```bash
$ sh shtool/br.sh
```

There are various other shell utility scripts included; you can alias them
by running
```bash
$ . shtool/setup.sh
# now you can just do "br" for build and run, "relbuild", etc
```

### Tests

Run the tests with "cargo test" or using the "rt.sh" utility script.  
If any tests fail, it is not recommended that you use this program.

### Configuration

Configuration is done by config file.  There are two default files, 
one for release and one for develop.  The release file is named 
"config.toml", and the develop file is named "config.dbg.toml".

GreyCrypt requires you to specify a list of directories that you want 
synced, and a "mapping" of some part of that directory to a keyword.
The keyword is used on other machines to figure out where to place 
the corresponding files.

See "config.sample.toml" for information on how to set up these mappings.

### Storage

In addition to your cloud provider directory, grey crypt stores 
sync state data in "~/.greycrypt" (Mac), or in "%appdata%\GreyCrypt"
(Windows).  No unencrypted file data or other identifying information 
is stored here.

### Application Lock Files

Some applications write temporary lock files to storage when a 
document is open for editing; these are to prevent multiple
instances of the application from opening the same file.  
There are two forms of this: the first uses exclusive write
locks so that no other process can write to the lock file; 
the second allows writes, and just puts information into the 
lock file indicating who is editing it.

GreyCrypt doesn't have any special knowledge of these files and
will attempt to sync them.  However, the first form 
(exclusive lock) will cause sync failures, because it won't be 
able to write the output file.  If you use a program that 
produces these lock files with GreyCrypt, it is recommended that
you add the lock file to the ignore list (see below).  You'll
then need to be careful that you don't overwrite the file from
two different machines with different data. 

The second form of lock is handled adequately by GreyCrypt.  
OpenOffice/LibreOffice is an example that uses this kind of lock file.

### Resolving conflicts

Occasionally a sync will produce conflicts; usually this is when 
a file with the same name and keyword mapping, but different contents,
is synced from two different computers.  

Right now you must resolve 
these manually.  Run greycrypt with the "-x" option, which will show
you a list of the conflicting files; remove one of the conflicting sync
files to remove the conflict.  You may need to remove or rename the
source local file on one machine to keep the conflict from recurring.

### Ignore list

Unlike the config file, the ignore list is shared between all 
systems that are syncing the same provider sync directory.  It is
stored in the root of that directory and is encrypted.  Because
it is encrypted, you need to use command line arguments to change it.

```
./target/debug/grey_crypt --show-ignore
# output omitted
./target/debug/grey_crypt --add-ignore '*.temp'
# output omitted
./target/debug/grey_crypt --remove-ignore '*.temp'
# output omitted
```

Each value is a unix shell style glob pattern.

### Password Daemon

It can be tedious to run multiple one-shot greycrypt commands because
it prompts you for a password each time.  If you are willing to tolerate
some insecurity, you can run a command with the -d option to spawn 
a password daemon that will remember the password for a period of time,
allowing you to run multiple commands with a prompt.  Run the 
program with -h for more details on how this works.

Currently the password daemon only works on unix.  

While it is running, the password daemon will hand out the password 
program to any program 
that know how to ask for it, though it does try to protect itself
from programs that aren't running as your user.  But generally its a 
security hole while it is running, so exercise caution. 

### Caveats and Limitations

* This is beta software and it is my first Rust program.  Its also not a 
backup program; keep backups independent of greycrypt files.  However,
I do use it with my own files.
* It uses filesystem polling (default 3 seconds), rather than 
events, so it is less efficient in CPU than it could be.
* It has not been tested with all cloud providers.  I have tested it with 
Google Drive and (to a lesser extent) Dropbox.
* It does not work on Linux, mainly because I have not decided how to deal
with its case-sensitive filesystem - GreyCrypt prefers case-insensitive
mode.  I welcome PRs that address this.
* I am not a crypto expert, so some parts of the implementation may be 
insecure.  I welcome an audit or suggestions from a trained crypto engineer.
* GreyCrypt can remove files; if you remove a file in a synced directory,
other systems that are syncing that file will remove it as well.
GreyCrypt uses the system trash/recycling bin, but its important to 
remember that this means the unencrypted file will sit in that bin 
for however long it takes the OS to remove it.  
* The command line options for conflict resolution are rough and 
not super useful.
* The cloud provider sometimes duplicates the sync directory,
see https://github.com/jmquigs/greycrypt/issues/2
